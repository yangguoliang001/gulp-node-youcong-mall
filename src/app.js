const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const router = require('./routes/router')
   
// var app = module.exports.app = exports.app = express();
const app = express()

app.use('/public/', express.static(path.join(path.resolve(__dirname, '..'), './dist/public/')))
app.use('/node_modules/', express.static(path.join(path.resolve(__dirname, '..'), './node_modules/')))
    // path.resolve(__dirname, '..')//当前命令所在的上一个目录
app.engine('html', require('express-art-template'))
app.set('views', path.join(path.resolve(__dirname, '..'), './dist/views/')) // 默认就是 ./views 目录

// parse application/x-www-form-urlencoded  
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())

app.use(router)

app.use(require('connect-livereload')());
app.listen(3000)