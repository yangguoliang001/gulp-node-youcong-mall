$(function() {

    var currentLi = true //当前li未被选中
    $('.left li').click(function() {
            // $(this).siblings().css('color', '#333')

            var index = $(this).index()
            if (index != 0 && currentLi) {
                // currentLi = false
                $(this).css('color', '#97C722')
                $(this).siblings().eq(0).css('color', '#333')
                var categoryAll = $(this).parent().siblings().text()
                var currentCategory = $(this).text()
                if (currentCategory != '全部') {
                    var label = categoryAll + currentCategory
                    var teml = `<span data-id='${index}'>${label}<i id = "del"> x </i></span>`
                        // console.log($('.search').children().attr('data-id'));
                    var flag = true
                    for (var i = 0; i < $('.search').children().length; i++) {
                        var searchId = Number($('.search span').eq(i).attr('data-id'))
                        if (searchId == index)
                            flag = false
                    }

                    if (flag) {
                        $('.header1 .search').append(teml)
                    }
                }
            }
            // else {
            //     currentLi = true
            //     $(this).css('color', '#333')
            //     for (var i = 0; i < $('.search').children().length; i++) {
            //         var searchIndex = Number($('.search').children().attr('data-id'))
            //         console.log(searchIndex, index);
            //         // if (index == searchIndex)
            //         // $('.search').children(['data-id' = index]).remove()

            //     }
            //     // console.log($('.search').children().eq(this));
            // }

        },

    )

    $('.header1 .search').on('click', '#del', function() {
        $(this).parent().remove()
        var index = $(this).parent().attr('data-id')
        var babel = $(this).parent().text().split('：')[0]
        var categories = $('.left i').text().split('：')
        for (var i = 0; i < categories.length - 1; i++) {
            if (babel == categories[i]) {
                $('.left ul').eq(i).children().eq(index).css('color', '#333')
                break
            }
        }
        var flag = true //当前没有被选中的li除'全部'选项
        for (var j = 1; j < $('.left ul').eq(i).children().length; j++) {
            if ($('.left ul').eq(i).children().eq(j).css('color') != 'rgb(51, 51, 51)') {
                flag = false
            }
        }
        if (flag)
            $('.left ul').eq(i).children().eq(0).css('color', '#97C722')


    })


    Ajax([])


    $('.nav2 #2 li').click(function() {
        var indexs = []
        var searchBabels = $('.header1 .search span')
            // console.log(searchBabels.length)
        for (var i = 0; i < searchBabels.length; i++) {
            var searchIndex = $(searchBabels[i]).attr('data-id')
            indexs.push(Number(searchIndex))
        }
        $('.header1 .search #del').click(function() {
            //     //为什么会重复输出2次，indexs
            var delItem = $(this).parent().attr('data-id')
            indexs = indexs.filter(function(item) {
                return item != delItem
            })
            console.log(indexs);
            $('.goods_box ul').empty()
            Ajax(indexs)

        })
        $('.goods_box ul').empty()
        Ajax(indexs)
    })

    //发送请求获取数据 
    function Ajax(indexs) {
        $.ajax({
            type: "get",
            url: "/getListPageDatas",
            dataType: "json",
            success: function(response) {
                if (indexs.length != 0) {
                    indexs.forEach((item) => {
                        var category = response[item].title
                        response[item][category].forEach((ele) => {
                            renderGoodsList(ele)
                        })
                    });
                } else {
                    for (var j = 1; j < response.length; j++) {
                        console.log(response.length);
                        var category = response[j].title
                        response[j][category].forEach((ele) => {
                            renderGoodsList(ele)
                        })
                    }
                }
            }
        });
    }


    function renderGoodsList(everyGoods) {
        var str = `
              <li>
                <a href="/productDetail/detail?id=${everyGoods.id}">
                    <img src="${everyGoods.imgUrl}" alt="">
                </a>
                <span class="text1">${everyGoods.displayName}</span>
                <span class="text2">${everyGoods.code}</span>
                <p>￥<em>${everyGoods.price}</em></p>

                <div class="addCarPurchase" data-id="${everyGoods.id}">
                    <span class="addCar">加入购物车</span>
                    <span class="purchase">立即购买</span>
                </div>
            </li>
        `
        $('.goods_box ul').append(str)
    }

})