// const { template } = require("express-art-template")

$(function() {

    //导航栏
    $('nav a').eq(0).mouseenter(function() {
        $('#aside').css('display', 'block')
    })
    $('nav a').eq(0).mouseleave(function() {
        $('#aside').css('display', 'none')
    })
    $('#aside').mouseenter(function() {
        $('#aside').css('display', 'block')

    })
    $('#aside').mouseleave(function() {
        $('#aside').css('display', 'none')
    })


    var iNow = 0;
    var timer = null;
    var olLis = $('#promo_nav li')
    var ul = $('.tb_promo ul')
    $('.tb_promo').mouseenter(function() {
        clearInterval(timer)

    })
    $('.tb_promo').mouseleave(function() {
        timer = setInterval(function() {
            iNow++
            tab()
        }, 3000)
    });
    olLis.click(function() {
        iNow = $(this).index()
        tab()
    })
    timer = setInterval(function() {
        iNow++
        tab()
    }, 3000)

    function tab() {
        olLis.removeClass('active').eq(iNow).addClass('active')
        if (iNow == olLis.length) {
            olLis.eq(0).addClass('active')
        }
        ul.animate({
            left: iNow * -$('.tb_promo ul li img').width()
        }, 500, function() {
            if (iNow == olLis.length) {
                iNow = 0
                ul.css('left', 0)
            }
        })
    }






    //猜我喜欢模块
    $('.title span').click(function() {
        $(this).siblings().css('color', 'black')
        $(this).css('color', '#83D347')
        var index = $(this).index()
        $('.goodsList').eq(index).css('display', 'block').siblings().css('display', 'none')
    })

    //品牌馆
    var liSize = $('.brand-container ul li').length
    var current = 1 //移动的次数
    $('.brand-container #prev').click(function() {
        var x = current * 240 //移动距离
        if (current <= liSize - 5) {
            $('.brand-container ul').stop().animate({ 'left': -x }, 500)
            current++
        }
    })
    $('.brand-container #next').click(function() {
        var x = 240 //移动距离
            //ul 距离左边的距离
        var leftX = parseInt($('.brand-container ul').css('left')) + x
        if (current > 1) {
            $('.brand-container ul').stop().animate({ 'left': leftX }, 500)
            current--
        }
    })

    //渲染页面-商品列表一

    $('.goodsContainer .title span').click(function() {
        var index = $(this).index()
        console.log(index);
        $('.goodsList ol').empty()
        Ajax(index)

    })


    Ajax(0)

    function Ajax(index) {
        $.ajax({
            type: "post",
            url: "/getIndexDatas",
            success: function(response) {
                for (var i = 0; i < response[index].length; i++) {
                    var str = `  <li>
                    <a href="/productDetail/detail?id=${response[index][i].id}">
                        <img src="${response[index][i].imgUrl}" alt="">
                    </a>
                    <span class="text1">${response[index][i].name}</span>
                    <span class="text2">${response[index][i].code}</span>
                    <p>￥<em>${response[index][i].price}</em></p>

                    <div class="addCarPurchase" data-id="${response[index][i].id}">
                        <span class="addCar">加入购物车</span>
                        <span class="purchase">立即购买</span>
                    </div>
                    </li>`
                    $('.goodsList ol').append(str)

                }
                for (var j = 0; j < response[2].length; j++) {
                    var str1 = `  <li>
                    <a href="/productDetail/detail?id=${response[2][j].id}">
                        <img src="${response[2][j].imgUrl}" alt="">
                    </a>
                    <span class="text1">${response[2][j].name}</span>
                    <span class="text2">${response[2][j].code}</span>
                    <p>￥<em>${response[2][j].price}</em></p>

                    <div class="addCarPurchase" data-id="${response[2][j].id}">
                        <span class="addCar">加入购物车</span>
                        <span class="purchase">立即购买</span>
                    </div>
                    </li>`
                    $('.hotSale ol').append(str1)

                }

                // console.log(response[index][0]);
                // var temp1 = template('indexListTeml', response[index][0])
                // console.log(temp1);
                // $('.goodsList ol').html(temp1)
                // var temp2 = template('indexListTeml', { indexGoods: response[2] })
                // console.log(temp2);
                // $('.hotSale').html(temp2)
            }
        });
    }



})