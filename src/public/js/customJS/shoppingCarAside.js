$(function() {
    //电梯导航

    $('#goback').click(function() {
            $('body,html').animate({
                scrollTop: 0
            }, 500);
            return false
        })
        //侧边购物车
    var timer = null
    $('#shoppingCar').mouseenter(function() {
        $('.asideCar').show()
    }).mouseleave(function() {
        timer = setTimeout(function() {
            $('.asideCar').hide()
        }, 2000)

    })
    $('.asideCar').mouseenter(function() {
        clearTimeout(timer)
        $('.asideCar').show()
    }).mouseleave(function() {
        $('.asideCar').hide()

    })
})