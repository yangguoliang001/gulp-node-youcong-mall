$(function() { 
    $('.p1 input[type="checkbox"]').click(function() {
        var checked1 = $('.p1 input[type="checkbox"]').is(':checked')
        if (checked1) {
            $('.cardContainer').css('display', 'block')
            $('.p1').css('marginTop', '0px')
            spouseCade()
        } else {
            $('.cardContainer').css('display', 'none')
            $('.p1').css('marginTop', '100px')
        }

    })

    //是否为配偶开卡
    function spouseCade() {
        $('.p2 input[type="checkbox"]').click(function() {
            var checked2 = $('.p2 input[type="checkbox"]').is(':checked')
            if (checked2) {
                $('.spouse').css('display', 'block')
            } else {
                $('.spouse').css('display', 'none')
            }
        })
    }

    $('.agreement input[type="checkbox"]').click(function() {
        // $(this).css('backgroundColor', '#65C722')
        var checked3 = $('.agreement input[type="checkbox"]').is(':checked')
        if (checked3) {
            $('#submit').css('backgroundColor', '#8DD753')
            $('#submit').attr('disabled', null)

        } else {
            $('#submit').css('backgroundColor', '#BDE89E')
            $('#submit').attr('disabled','disabled')
        }
    })


    $('.codeContainer .sendCode').click(function () {
        var phone = $('#phone').val()

        var pattern = /^1(3|4|5|6|7|8|9)\d{9}$/

        if (!pattern.test(phone)) {
            $('#tip span').text('请输入正确的11位手机号码')
            tipAnimate()
        }
        
    })


    //提示框动画效果
    function tipAnimate() {
        $('#tip').stop().animate({
            top: 100
        }, 500, function () {
            setTimeout(function () {
                $('#tip').stop().animate({
                    top: -80
                }, 500)
            }, 1000)

        })
    }

    $('#submit').click(function (e) {
        // if(this).attr('disabled')
        var data = ''
        data += $('#phone').serialize() + "&"
        data += $('#inputCode').serialize()

        $.ajax({
            type: "post", 
            url: "/userRegister",
            data: data,
            dataType: "json",
            success: function (response) {
                console.log(response);

                // if (response.err_code == 1) {
                //     $('#tip span').text(response.message)
                //     $('#tip i').text('x').css('backgroundColor', 'red')
                //     tipAnimate()
                // }
                $('#tip span').text(response.message)
                $('#tip i').text('!').css('backgroundColor', '#FBB514')
                tipAnimate()
                

            }
        });
    })


})