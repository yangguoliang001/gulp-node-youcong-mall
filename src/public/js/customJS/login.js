$(function() { 
    $('.loginContainer h4 span').click(function() {
        var index = $(this).index()
        if (index == 0) {
            $('.loginContainer1').show()
            $('.loginContainer2').hide()
            $(this).addClass('spanStyle')
            $(this).siblings().removeClass('spanStyle')

        } else if (index == 1) {
            $('.loginContainer1').hide()
            $('.loginContainer2').show()
            $(this).addClass('spanStyle')
            $(this).siblings().removeClass('spanStyle')
        }
    })
    $('#isMember').click(function() {
        $('.loginContainer').show()
        $('.noMemberLogin').hide()
        $('.loginContainer h4 span').eq(0).addClass('spanStyle')
        $('.loginContainer h4 span').eq(0).siblings().removeClass('spanStyle')
    })
    $('#isNot').click(function() {
        $('.loginContainer').hide()
        $('.noMemberLogin').show()
    })
    $('#isNot2').click(function() {
        $('.loginContainer').hide()
        $('.noMemberLogin').show()
        $('.loginContainer1').show()
        $('.loginContainer2').hide()

    })

    $('.codeContainer .sendCode').click(function(){
        var phone = $('#phone').val()

        var pattern = /^1(3|4|5|6|7|8|9)\d{9}$/

        if (!pattern.test(phone)){
            $('#tip span').text('请输入正确的11位手机号码')
            tipAnimate()
        }
        // else{
        //     $('#tip span').text('手机未注册')
        //     $('#tip i').text('x').css('backgroundColor','red')
        //     tipAnimate()
        // }
    })  


    //提示框动画效果
    function tipAnimate(){
        $('#tip').stop().animate({
            top: -100
        }, 500, function () {
            setTimeout(function () {
                $('#tip').stop().animate({
                    top: -250
                }, 500)
            }, 1000)

        })
    }
    //登录界面信息判断


    //提交数据
    $('.loginContainer .loginContainer1 .submit').click(function (e) {
        var data = ''
        data += $('#phone').serialize()+"&"
        data += $('#inputCode').serialize() + "&"
        data += $('#inpCode').serialize()
        $.ajax({
            type: "post",
            url: "/memberLogin1",
            data: data, 
            dataType: "json",
            success: function (response) {
                console.log(response);

                if(response.err_code == 1){
                    $('#tip span').text(response.message)
                    $('#tip i').text('x').css('backgroundColor','red')
                    tipAnimate()
                } else if(response.err_code == 0){
                    $('#tip span').text(response.message+'    马上跳转首页')
                    $('#tip i').text('√').css('backgroundColor', 'springgreen')
                    tipAnimate()
                    setTimeout(function(){
                        window.location.href = '/'  
                    },1000) 
                }
                else{
                    $('#tip span').text(response.message)
                    $('#tip i').text('!').css('backgroundColor', '#FBB514')
                    tipAnimate()
                }
                
            }
        });
    })


})