const fs = require('fs');
const path = require('path');
// getIndexData()

exports.getGoodsAllData = function(callback) {
    var jsonPath = path.resolve(__dirname, '..')
    fs.readFile(`${jsonPath}/goodsAllData.json`, 'utf8', function(err, res) {
        if (err) throw err;
        callback && callback(JSON.parse(res))
    })
};