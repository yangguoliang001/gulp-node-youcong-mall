const fs = require('fs');
const path = require('path');
// getIndexData()

exports.getListPageData = function(callback) {
    var jsonPath = path.resolve(__dirname, '..')
    fs.readFile(`${jsonPath}/goodsListAll.json`, 'utf8', function(err, res) {
        if (err) throw err;
        // console.log(res);
        callback && callback(JSON.parse(res))
    })
};