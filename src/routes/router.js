const express = require('express');
const getIndexData = require('../models/js/getIndexData')
const getListPageData = require('../models/js/getListPageData')


// console.log(getIndexData.getIndexData);
// console.log(getIndexData.getIndexData);

const detailPageRouter = require('./getDetailPage')
const getAllDatas = require('./getAllDatas')
const shoppingCarRouter = require('./shoppingCar')
const loginRegiRouter = require('./loginAndRegister')



const router = express.Router()


router.get('/', function(req, res) {
    res.render('index.html')
})



router.get('/productlist/productlist', function(req, res) {
    res.render('goodsList.html')
})

router.post('/getIndexDatas', function(req, res) {
    getIndexData.getIndexData(function(datas) {
        res.send(datas)
    })
})


//获取列表页数据
router.get('/getListPageDatas', function(req, res) {
    getListPageData.getListPageData(function(datas) {
        res.send(datas)
    })
})



router.use(detailPageRouter)

router.use(getAllDatas)

router.use(shoppingCarRouter)

router.use(loginRegiRouter)

module.exports = router