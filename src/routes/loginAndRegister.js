const express = require('express');


const mysql = require('../models/js/mysql')
const loginRegiRouter = express.Router()



loginRegiRouter.get('/login', function (req, res) {
    res.render('login.html')
})
loginRegiRouter.get('/register', function (req, res) {
    res.render('register.html')
})
loginRegiRouter.post('/memberLogin1', function (req, res) {
    var userPhone = req.body.tel
    mysql.searchUser(function(err,result){
         
        if(err){
           res.status(500).json({
               err_code: 500,
               message: err.message
           })
        }
        else{
            var pattern = /^1(3|4|5|6|7|8|9)\d{9}$/
            if (!pattern.test(userPhone)) {
                res.status(200).json({
                    err_code: 2,
                    message: '请正确输入手机号'
                })
            }else{
                var isUser = false;//假设用户bu存在
                var id = 0 //记录当前用户信息在数据库中所处的id
                for (var i = 0; i < result.length; i++) {
                    if (userPhone == result[i].phone) {
                        isUser = true
                        id = result[i].id
                    }
                }
                if (isUser) { //如果用户手机号已注册
                    //判断验证码

                    var phoneCode = req.body.phoneCode
                    if (phoneCode.length == 6){
                        //判断会员卡号
                        var cardCode = req.body.cardCode
                        if (!cardCode){
                            res.status(200).json({
                                err_code: 4,
                                message: '请输入要登录的会员卡号'
                            })
                        }
                        else if (cardCode){
                            console.log(id)
                            if (cardCode == result[id-1].cardCode) 
                                res.status(200).json({
                                    err_code: 0,
                                    message: '登录成功'
                                }) 
                            else
                                res.status(200).json({
                                    err_code: 5,
                                    message: '会员卡号输入错误'
                                })
                        }
                    } else if (phoneCode.length!=6){
                        res.status(200).json({
                            err_code: 3,
                            message: '请输入正确验证码'
                        })
                    }

                } else {
                    res.status(200).json({
                        err_code: 1,
                        message: '手机未注册'
                    })
                }
            }
        

        }
       
    })


})



loginRegiRouter.post('/userRegister', function (req, res) {
    var userPhone = req.body.tel
    mysql.searchUser(function (err, result) {

        if (err) {
            res.status(500).json({
                err_code: 500,
                message: err.message
            })
        }
        else {
            var pattern = /^1(3|4|5|6|7|8|9)\d{9}$/
            if (!pattern.test(userPhone)) {
                res.status(200).json({
                    err_code: 2,
                    message: '请正确输入手机号'
                })
            } else {
                var isUser = false;//假设用户bu存在
                var id = 0 //记录当前用户信息在数据库中所处的id
                for (var i = 0; i < result.length; i++) {
                    if (userPhone == result[i].phone) {
                        isUser = true
                        id = result[i].id
                    }
                }
                if (isUser) { //如果用户手机号已注册
                    //判断验证码
                    res.status(200).json({
                        err_code: 5,
                        message: '手机号已被注册'
                    })  
                } else {
                    var phoneCode = req.body.code
                    if (phoneCode.length != 6) {
                        res.status(200).json({
                            err_code: 3,
                            message: '请输入正确验证码'
                        })
                    }else{
                        //将用户数据插入数据库
                    }
                }
            }


        }

    })


})

module.exports = loginRegiRouter