const gulp = require('gulp');
const htmlmin = require('gulp-htmlmin');
const rename = require('gulp-rename');
const cssmin = require('gulp-cssmin')
const autoPrefixer = require('gulp-autoprefixer')
const gulpscss = require("gulp-sass");
const gulpless = require("gulp-less");
const minifycss = require("gulp-minify-css");
const { dest } = require('gulp');
const uglify = require("gulp-uglify");
const babel = require("gulp-babel");
const concat = require("gulp-concat");
// const removeUseStrict = require("gulp-remove-use-strict");
// const connect = require("gulp-connect");
// const server = require('gulp-express');

// 调用 .create() 意味着你得到一个唯一的实例并允许您创建多个服务器或代理。 
var browserSync = require('browser-sync').create();

// 这里reload不加括号，只引用不调用
var reload = browserSync.reload;

//压缩index.html代码 ?(html|htm)
function indexhtml() {
    return gulp
        .src('./src/views/**/*.html')
        .pipe(htmlmin({
            removeEmptyAttributes: true,
            collapseWhitespace: true
        }))
        // .pipe(rename('index.html'))
        .pipe(gulp.dest('./dist/views'))
        // .pipe(connect.reload());

    .pipe(reload({ stream: true }));
    // .pipe(server.notify())
}




function indexCSS() {
    return gulp.src('./src/public/css/index.css')
        .pipe(autoPrefixer({
            "overrideBrowserslist": [
                "defaults",
                "not IE 11",
                "not IE_Mob 11",
                "maintained node versions"
            ]
        }))
        .pipe(cssmin())
        // .pipe(rename("index.min.css"))
        .pipe(gulp.dest('./dist/public/css'))
        .pipe(reload({ stream: true }));

    // .pipe(server.notify())

}

function indexScss() {
    return gulp
        .src('./src/public/css/*.scss')
        .pipe(gulpscss())
        .pipe(minifycss())
        // .pipe(rename("index.min.css"))
        .pipe(gulp.dest("dist/public/css"))
        .pipe(reload({ stream: true }));

    // .pipe(server.notify())

}

//less文件压缩
function indexLess() {
    return gulp
        .src('./src/public/css/index.less')
        .pipe(gulpless())
        .pipe(minifycss())
        // .pipe(rename("index.min.css"))
        .pipe(gulp.dest("dist/public/css"))
        .pipe(reload({ stream: true }));

    // .pipe(connect.reload());
}

//图片复制
function copy_images() {
    return gulp
        .src('./src/public/images/**/*')
        .pipe(gulp.dest('dist/public/images'))
        .pipe(reload({ stream: true }));

}

//字体图标复制
function copy_font() {
    return gulp
        .src('./src/public/iconfont/*')
        .pipe(gulp.dest('dist/public/iconfont'))
        .pipe(reload({ stream: true }));

}
gulp.task(copy_font)

function json_copy() {
    return gulp
        .src('./src/models/*.json')
        .pipe(gulp.dest('dist/models'))
        .pipe(reload({ stream: true }));

    // .pipe(server.notify())

}

//自定义JS文件打包
function js_files() {
    return gulp
        .src('./src/public/js/customJS/*.js')
        // .pipe(removeUseStrict())
        .pipe(
            babel({
                presets: ["@babel/preset-env"],
            })
        )
        .pipe(uglify())
        .pipe(gulp.dest('./dist/public/js/customJS'))
        .pipe(reload({ stream: true }));

    // .pipe(connect.reload());
    // .pipe(server.notify())
}
gulp.task(js_files)
    //路由文件打包
function routerJS() {
    return gulp
        .src('./src/routes/*.js')
        // .pipe(removeUseStrict())
        .pipe(
            babel({
                presets: ["@babel/preset-env"],
            })
        )
        .pipe(uglify())
        .pipe(gulp.dest('./dist/routes/'))
        .pipe(reload({ stream: true }));

    // .pipe(connect.reload());
    // .pipe(server.notify())
}
//获取数据
function datasJS() {
    return gulp
        .src('./src/models/js/*.js')
        // .pipe(removeUseStrict())
        .pipe(
            babel({
                presets: ["@babel/preset-env"],
            })
        )
        .pipe(uglify())
        .pipe(gulp.dest('./dist/models/js'))
        .pipe(reload({ stream: true }));

    // .pipe(connect.reload());
    // .pipe(server.notify())
}

function appJs() {
    return gulp
        .src('./src/app.js')
        // .pipe(removeUseStrict())
        .pipe(
            babel({
                presets: ["@babel/preset-env"],
            })
        )
        .pipe(uglify())
        .pipe(gulp.dest('./dist'))
        .pipe(reload({ stream: true }));

    // .pipe(connect.reload());
    // .pipe(server.notify())
}

function thirdPartyJS() {
    return gulp
        .src('./src/public/js/thirdPartyJS/*.js')
        .pipe(gulp.dest('./dist/public/js/thirdPartyJS'))
        .pipe(reload({ stream: true }));

    // .pipe(server.notify())
}

function watch_items() {
    // gulp.watch("./src/index.?(html|htm)", indexhtml);
    // gulp.watch("./src/images/**/*", copy_images);
    // gulp.watch(["./src/css/index.css"], indexCSS);
    // gulp.watch("./src/css/index.less", indexLess);
    // gulp.watch(["./src/css/index.scss"], indexScss);
    // gulp.watch(["./src/js/customJS/*.js"], js_files);
    // gulp.watch(["./src/app.js"], appJs);
    // gulp.watch(["./src/model/*.json"], json_copy);
    // gulp.watch(["./src/js/thirdPartyJS/*.js"], thirdPartyJS);
    return new Promise(function(resolve, reject) {
        gulp.watch("./src/views/**/*.html", gulp.series(indexhtml));
        gulp.watch("./src/public/images/**/*", gulp.series(copy_images));
        gulp.watch(["./src/public/css/index.css"], gulp.series(indexCSS));
        // gulp.watch("./src/public/css/index.less", gulp.series(indexLess));
        gulp.watch(["./src/public/css/*.scss"], gulp.series(indexScss));
        gulp.watch(["./src/public/js/customJS/*.js"], gulp.series(js_files, ));
        gulp.watch(["./src/app.js"], gulp.series(appJs));
        gulp.watch(["./src/models/*.json"], gulp.series(json_copy));
        gulp.watch(["./src/public/js/thirdPartyJS/*.js"], gulp.series(thirdPartyJS));
        gulp.watch(["./src/routes/*.js"], gulp.series(routerJS));
        gulp.watch(["./src/public/iconfont/*"], gulp.series(copy_font));
        gulp.watch(["./src/models/js/*.js"], gulp.series(datasJS));

        resolve()
    })

}

// function server(){
//     return new Promise(function(resovle,reject){
//         connect.server({
//             root: "dist",
//             port: 3000,
//             livereload: true,
//         });
//         resovle()
//     })

// }
// exports.build = gulp.parallel(
//     indexhtml, copy_images, indexCSS, indexLess, indexScss, js_files, appJs, json_copy, thirdPartyJS
// )


// function server1(){
//     return new Promise(function(resolve,reject){
//         server.run(['./src/app.js'])
//         resolve()
//     })
// }




var nodemon = require('gulp-nodemon');

function server1() {
    return new Promise(function(resolve, reject) {
        nodemon({
            script: './dist/app.js',
            // 忽略部分对程序运行无影响的文件的改动，nodemon只监视js文件，可用ext项来扩展别的文件类型 
            ignore: ["gulpfile.js", "node_modules/", "./dist/public/**/*.*"],
            env: { 'NODE_ENV': 'development' }
        }).on('start', function() {
            browserSync.init({
                    proxy: 'http://localhost:3000',
                    files: ["./dist/public/**/*.*", "./src/views/**", "./dist/routes/**"],
                    port: 3001
                },
                function() {
                    console.log("browser refreshed.");
                });
        });
        resolve()
    })
};
// exports.default = gulp.parallel(watch_items,server1,)
exports.default = gulp.parallel([server1, done => done()], [watch_items, done => done()])
    // exports.default = gulp.parallel([server, done => done()], [watch_items, done => done()])